const socketIO = require('socket.io');
const Document = require('../models/Document');

exports.sio = (server) => {
  return socketIO(server, {
    transport: ['polling'],
    cors: {
      origin: '*',
    },
  });
};

exports.connection = (io) => {
  io.on('connection', socket => {
    socket.on('get-document', async (documentId) => {
      const document = await findOrCreateDocument(documentId);
      socket.join(documentId);
      socket.emit('load-document', document.data);

      socket.on('send-changes', delta => {
        socket.broadcast.to(documentId).emit('receive-changes', delta);
      });

      socket.on('send-range-changes', range => {
        socket.broadcast.to(documentId).emit('receive-range-changes', range);
      });

      socket.on('save-document', async (data) => {
        await Document.findByIdAndUpdate(documentId, { data: data.quill, publisher: data.publisher });
      });
    });
  });
};

const defaultValue = '';

async function findOrCreateDocument(id) {
  if (id === null) return;

  const document = await Document.findById(id);
  if (document) return document;

  return await Document.create({ _id: id, data: defaultValue });
};
