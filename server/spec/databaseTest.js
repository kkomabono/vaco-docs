const request = require('supertest');
const { expect } = require('chai');
const app = require('../app');
const agent = request.agent(app);

describe('Use MongoDB database', function() {
  this.timeout(5000);

  const mongoose = require('mongoose');
  const db = mongoose.connection;

  before((done) => {
    (function checkDatabaseConnection() {
      if (db.readyState === 1) {
        return done();
      }

      setTimeout(checkDatabaseConnection, 1000);
    })();
  });

  describe('01. database test', () => {
  });
});
