const express = require('express');
const router = express.Router();
const verifyToken = require ('../middleware/auth');
const pageController = require('./controllers/page.controllers');

router.get('/', verifyToken, pageController.userDocumentList);
router.delete('/', verifyToken, pageController.deleteDocument);

module.exports = router;
