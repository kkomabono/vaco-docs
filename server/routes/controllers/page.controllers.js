const Document = require ('../../models/Document');

exports.userDocumentList = async (req, res, next) => {
  try {
    const documentData = await Document.find({ publisher: req.userData.email });
    res.json(documentData);
  } catch (error) {
    res.send({ message: '서버에러' });
    next(error);
  }
};

exports.deleteDocument = async (req, res, next) => {
  try {
    await Document.findByIdAndDelete(req.body.documentId);
    res.json({ message: 'success' });
  } catch (error) {
    res.send({ message: '서버에러' });
    next(error);
  }
};
