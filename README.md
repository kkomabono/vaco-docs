# 배포주소
https://vaco-docs.herokuapp.com/

# Vaco-Docs

![Docs](./README_assests/vaco-docs-page.png)

**React + Express + MongoDB + NodeJS + Socket.io**를 이용한 실시간 문서편집기입니다.

## 설치방법

```sh
npm install
```

## 실행을 위한 환경변수 설정
1. client의 루트디렉토리에 .env에 환경변수 저장.
- REACT_APP_API_KEY=AIzaSyBP_fgdhukMDrnlQtAZ9e-hpmcRz-2eYxA
- REACT_APP_AUTH_DOMAIN=vaco-docs-a77ce.firebaseapp.com
- REACT_APP_PROJECT_ID=vaco-docs-a77ce
- REACT_APP_STORAGE_BUCKET=vaco-docs-a77ce.appspot.com
- REACT_APP_MESSAGING_SENDER_ID=123012916436
- REACT_APP_ID=1:123012916436:web:1e523402cc1d3f0aa98741
- REACT_APP_MEASUREMENT_ID=G-3PS564R1ME
- REACT_APP_BASE_URL=http://localhost:8000

2. server의 루트디렉토리에 .env에 환경변수 저장.
- MONGO_URI=mongodb+srv://jkw8282:1234@cluster0.6n2sz.mongodb.net/vaco-docs?retryWrites=true&w=majority
- PORT=8000

3. firebase-admin을 사용하기 위한 SDK설정 파일 'vaco-docs-admin.json' 은 server의 루트디렉토리에 위치한다.

## 실행방법
/vaco-docs 에서

```sh
npm run dev
```
or

터미널에서 /vaco-docs/client폴더로 이동 후
```sh
npm run start
```
이어서 /vaco-docs/server폴더로 이동 후
```sh
npm run start
```
***주의! 두 번째 방법으로 실행 시 터미널 두개에서 각각 실행하세요.***

***주의! react: 3000번포트, express: 8000번포트를 사용하므로 겹치는 서버주소를 사용하고 있다면 반드시 해당 주소는 끄고 실행하세요.***

## 사용된 툴
1. MongoDb
2. React
3. Express
4. Nodejs
5. Socket.io
6. Quill

## 로그인인증과 Authorization체크

1. 구글 firebase를 이용한 google OAuth 소셜로그인과 이메일과 비밀번호를 이용한 회원가입/로그인 구현.
2. firebase에서 제공한 JWT토큰을 이용한 사용자 인증.

## 핵심기능
- [x] 구글 firebase를 이용한 로그인 및 인증
- [x] 로그인 한 사용자는 새로운 문서 생성 가능
- [x] 생성된 문서는 최초생성 후 20초마다 자동저장 혹은 수동으로 저장 기능
- [x] 문서의 고유 ID로 여러명이 동시에 작업하는 기능
- [x] 문서페이지에서 문서삭제 가능
- [x] firebase-admin과 JWT토큰을 이용한 유저 확인기능
- [x] 동시에 여러명이 한 문서에서 작업 할 경우 다른 작업자의 커서 확인가능
- [x] 본인이 생성한 문서리스트 확인 기능

## Vaco-docs의 폴더 및 컴포넌트 구조
1. client: React를 이용한 프론트엔드 파일 내장
2. client/src: 컴포넌트와 firebase인증관련 핵심파일
3. server: express서버 파일 내장
4. server/routes: 서버 라우팅 파일 내장
5. server/middleware: 유저 인증관련 파일 내장
6. server/utils: socket.io 관련 파일 내장
7. server/config: 서버 설정파일 내장

## CORS정책 해결
proxy를 이용하기 위해 http-proxy-middleware를 사용하였다. 서버는 로컬 8000번 포트, 리액트는 로컬 3000번 포트를 사용한다.

## 새롭게 시도해본 점
1. styled components를 적극 이용해서 스타일을 적용하였다.  그 동안은 css파일에 모든 스타일을 작성해서 어떤 컴포넌트에 무슨 스타일이 적용되어 있는지 알아보기 힘들었는데 이제 알아보기 상당히 편해졌다.
2. axios를 통해 서버요청을 구현하였다.  fetch대신 axios를 사용한 이유는 fetch를 사용할 경우에는 받아온 데이터에 또 다시 json()으로 파싱해야 하기 때문에 번거로운 점이 있었다.  
또한 axios의 interceptors기능을 이용한 에러핸들링, 서버에 요청시 일일이 header를 입력하지 않아도 되는 점에서 이점이 있어 axios를 사용하였다.
3. react-query를 사용하였으나 최종적으로는 삭제하였다.  react-query를 사용하면 캐시기능을 통해 일정시간마다 state의 변화를 감지해서 화면에 출력시켜준다.  
react-query를 쓴다면 클라이언트가 서버와 통신하지 않더라도 캐시를 읽어서 클라이언트에 화면을 출력시켜줄 수 있다. 이는 서버와의 통신을 줄여서 부하를 줄일 수 있고 클라이언트에도 DB를 사용하는 것과 같은 효과를 가질 수 있다.  
그러나 이번 과제에서는 관리해야 할 상태가 많지 않았고 실시간으로 정보를 최신화시켜주어야 하는 부분은 socket.io로 통신하였기 때문에 최종적으로 react-query는 사용하지 않게 되었다.
4. quill 텍스트에디터를 사용하였다.  quill텍스트에디터는 기본적으로 제공되는 강력한 문서편집기능과 다양한 API로 사용자에게 많은 편의를 제공해준다.  
처음에는 div태그의 'contenteditable' 속성으로 div태그 내부의 텍스트를 가져왔지만 이전의 변경사항, 새롭게 추가된 사항을 모두 작성하려면 번거로운 작업이 많았다.  
quill의 API중 setContent, updateContents, on API를 통해서 기존의 div태그를 사용했던 작업과 비교해서 훨씬 간편하게 실시간 문서편집기능을 구현할 수 있었다.

## 어려웠던 점
1. firebase를 이용한 로그인, 인증을 처음 사용해 보았는데 기존의 passport를 사용한 방법보다 훨씬 추상화가 많이 되어있어서 어떤 방식으로 동작하는지 이해하기가 어려웠다.  
firebase의 강력한 기능을 이용하기 위해서는 API를 적재적소에 사용해야 하는데 아무래도 처음 사용하다보니 어떤 API를 어디에, 어떻게 적어야 하는지 익숙하지 않다보니 구현하기가 어려웠다.  
특히 firebase의 최신 버전은 9버전인데, 이전의 버전들과 동작방식이 다른 API들이 다수 있어서 적절한 방법으로 실행시키는 것이 어려웠다.  
그러나 firebase공식문서를 보고 구글링을 통해 다양한 API의 사용방법과 기능들을 알게 되었고 최종적으로는 구글 소셜로그인과 이메일/비밀번호를 이용한 회원가입과 로그인을 구현하였다.  
조금 익숙해지고 보니 passport.js를 이용한 인증보다 코드의 양이 훨씬 적고 간편하게 사용할 수 있었다.
차후에 있을 프로젝트에서는 firebase와 구글의 다양한 API를 통해 프로젝트를 수행하고자 한다.
2. 위의 firebase와 마찬가지로 firebase-admin을 통한 유저인증에도 많은 시간을 소요하였다.  
firebase는 클라이언트에서 로그인을 수행하고 firebase자체에서 jwt토큰을 발행해준다.  이 토큰으로 내 서버와 통신할 때 인증을 수행할 수 있는데, 이 때 필요한 것이 firebase-admin이다.  
firebase-admin을 사용하기 위해서는 먼저 firebase SDK를 추가해 주어야 하는데 이 부분에서 많은 시간이 소요되었다.  
SDK를 추가하기 위해서는 admin.json파일을 시스템에 환경변수로 import시켜주는 방법과 로컬에 import해주는 방법이 있다. 이번 과제에서는 로컬에 admin.json파일을 import시켜주었다.
3. socket.io를 이용한 실시간 통신을 개념적으로 이해하는데 많은 시간이 들었다.  나름대로 이해한 socket.io는 같은 작업을 요청하는 사용자들에게 하나의 공간을 만들어주고 해당 공간에서의 정보들은 모두 공유되는 편리한 통신이다.  
socket.io 구현에 있어서 가장 어려웠던 점은 역시 개념적인 부분에서 나타났다. 이미 express로 서버를 만들었는데 socket이라는 서버를 또 다른 로컬포트에 만들어주어야 한다고 생각하고 react, express, socket 3개의 포트를 사용한다고 생각하였다. 그러나 socket은 기존 서버를 업그레이드 시킨다 생각하고 기존 express에 socket을 얹어주었더니 잘 동작하였다.  
socket을 클라이언트에서 사용하기 위해서는 socket.io-client 패키지를 사용해서 서버에 socket연결을 요청하고 서버가 만든 공간에 해당 사용자를 넣어주어야 한다.
이때 CORS문제가 발생하게 되는데 socket.io를 설정해주는 부분에서 cors: { origin: * }을 넣어줌으로써 해결하였다.
4. CRA를 사용해서 react프로젝트를 시작하는 것 처럼 express도 express를 기본적으로 만들어주는 express generator가 있다.  
그러나 socket.io를 사용하기 위해서는 http와 app에 설정해주어야 하는데 express generator를 사용하면 bin/www 에서부터 서버가 시작되므로 둘을 동기화시켜주는 것이 어려웠다.  
react나 express를 편리하게 만들 수 있는 장점이 있지만 기본으로 들어있는 패키지들이 많아서 용량이 커지고 로딩이 느려질 수도 있는 단점도 있다.  
앞으로의 개발에서는 어떤 방식으로 프로젝트를 처음 시작할 지 잘 설계하는 것도 중요하다고 느낀 점이다.

## 아쉬웠던 점, 개선해야할 점
1. 시간이 촉박하다보니 우선순위를 잘 따지지 못해서 더 할 수 있었는데도 불구하고 만족할만한 수준으로 해 내지 못한 것 같아 아쉽다.  
2. 백엔드 공부를 할 때 supertest로 나름 테스트코드를 잘 써서 이번 과제에서도 여유롭게 쓸 수 있을거라 생각했다.  
그러나 firebase로 인증, socket.io도입, 백엔드와 프론트엔드의 통합으로 인해 막상 테스트 작성이 잘 되지 않았다.  
TDD로 코딩을 하기 위해서는 테스트작성이 필수인데 테스트작성을 하지 못한것이 제일 아쉬운 부분이다.  
과제제출 이후에도 socket.io테스트, firebase테스트, 마지막으로 백엔드와 프론트엔드 통합테스트를 작성 할 예정이다.
