import React, { useEffect, useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import styled from 'styled-components';
import firebase from './service/firebase';

import Login from './common/components/Login';
import Home from './common/components/Home';
import DocumentPage from './common/components/DocumentPage';
import MyList from './common/components/MyList';
import Register from './common/components/Register';
import AppHeader from './common/components/AppHeader';
import Error from './common/components/Error';

function App() {
  const [user, setUser] = useState(null);
  const [token, setToken] = useState(null);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      setUser(user);
    });
  }, []);

  useEffect(() => {
    if (user) {
      firebase.auth().currentUser.getIdToken(true).then(function(token) {
        setToken(token);
      });
    }
  }, [user]);

  return (
    <>
      <AppHeader />
      <Container>
        <Routes>
          <Route path='/' element={user ? <Home user={user} setToken={setToken} /> : <Login />} />
          <Route path='/documents'>
            <Route path='my-list' element={user ? <MyList token={token} /> : <Login />} />
            <Route path=':id' element={user ? <DocumentPage token={token} user={user} /> : <Login />} />
          </Route>
          <Route path='/register' element={<Register />} />
          <Route path='/error' element={<Error />} />
        </Routes>
      </Container>
    </>
  );
}

export default App;

const Container = styled.div`
  margin: 0 20vw;
`;
