import moment from 'moment';

const transformTimeFormat = (time) => {
  const momentDate = moment(time);

  return momentDate.format('YYYY-MM-DD HH:mm:ss');
};

export default transformTimeFormat;
