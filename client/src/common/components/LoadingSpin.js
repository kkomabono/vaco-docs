import * as Loader from 'react-loader-spinner';

export default function LoadingSpin() {
  const style = {
    position: "fixed",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  };

  return (
    <div style={style}>
    <Loader.TailSpin
      type='Oval'
      color='#32A4FF'
    />
    </div>
  );
}

