import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import { auth } from '../../service/firebase';
import firebase from '../../service/firebase';

import { v4 as uuid } from 'uuid';
import Button from './shared/Button';

export default function Home({ user, setToken }) {
  const navigate = useNavigate();

  const handleNewDocumentButton = () => {
    navigate(`/documents/${uuid()}`);
  };

  const handleMyListButton = () => {
    navigate('/documents/my-list');
  };

  useEffect(() => {
    firebase.auth().currentUser.getIdToken(true).then(function(token) {
      setToken(token);
    });
  }, []);

  return (
    <>
      <div className='home'>
        <h1>
          Welcome,
          <span>
            {user.displayName}<br></br>
          </span>
        </h1>
      </div>
      <div>
        <Button onClick={handleNewDocumentButton}>New document</Button>
      </div>
      <div>
        <Button onClick={handleMyListButton}>My document</Button>
      </div>
      <div>
        <Button onClick={() => auth.signOut()}>Sign out</Button>
      </div>
    </>
  );
}
