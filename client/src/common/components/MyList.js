import React, { useState, useEffect, Fragment } from 'react';
import { useNavigate } from 'react-router-dom';

import styled from 'styled-components';
import transformTimeFormat from '../utils/transformTimeFormat';
import axios from 'axios';
import '../../api/api';

import Button from './shared/Button';
import LoadingSpin from './LoadingSpin';

export default function MyList({ token }) {
  const [userDocuments, setUserDocuments] = useState(null);
  const navigate = useNavigate();

  try {
    useEffect(() => {
      if (token) {
        const fetchData = async () => {
          const fetchedData = await axios.get('/page', {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });

          setUserDocuments(fetchedData);
        }

      fetchData();
      }
    }, [token]);
  } catch (error) {
    console.error(error);
  }

  const handleMoveToDocument = (id) => {
    navigate(`/documents/${id}`);
  };

  useEffect(() => {
    const item = document.querySelectorAll('.fancy-item');
    let count = 0;

    if (item.length) {
      const addActiveClass = () => {
        item[count].classList.add('active');
        count++;

        if (count >= item.length) {
          clearInterval(addActive);
        }
      }

      const addActive = setInterval(addActiveClass, 100);
    }
  }, [userDocuments]);

  const handleMoveMainPage = () => {
    navigate('/');
  };

  return (
    <div>
      <h1> My documents</h1>
      <Button onClick={handleMoveMainPage}>To main page</Button>
      {userDocuments === null ? (
        <LoadingSpin />
      ) : (
        userDocuments.data.map((document) => (
          <Fragment key={document._id}>
            <DocumentListWrapper
              onClick={() => handleMoveToDocument(document._id)}
            >
              <DocumentItem className='fancy-item' onClick={() => handleMoveToDocument(document._id)}>
                <div>
                  {document.data.ops[0].insert}
                </div>
                <div className='time-stamp'>
                  Last modified in {transformTimeFormat(document.updatedAt)}
                </div>
              </DocumentItem>
            </DocumentListWrapper>
          </Fragment>
        ))
      )}
    </div>
  );
}

const DocumentListWrapper = styled.div`
  cursor: pointer;
  font-size: 20px;
  padding: 50px 20px;
  border-bottom: 1px solid #eee;
  transition-duration: 0.4s;

  &:hover {
    background: #eee;
  }

  .fancy-item {
    margin-top: 50px;
    opacity: 0;
    transition: 0.3s ease-in;
  }

  .fancy-item.active {
    margin-top: 0;
    opacity: 1;
  }
`;

const DocumentItem = styled.div`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  .time-stamp {
    font-size: 16px;
    color: #969696;
  }
`;
