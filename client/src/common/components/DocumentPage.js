import React, { useCallback, useState, useRef, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { io } from 'socket.io-client';
import { auth } from '../../service/firebase';

import Quill from 'quill';
import QuillCursors from 'quill-cursors';
import 'quill/dist/quill.snow.css';

import styled from 'styled-components';
import axios from 'axios';

import Button from './shared/Button';

export default function DocumentPage({ user, token }) {
  const [socket, setSocket] = useState();
  const [quill, setQuill] = useState();
  const [cursor, setCursor] = useState();
  const socketIO = useRef();
  const { id: documentId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    socketIO.current = io('ws://localhost:8000');
    setSocket(socketIO.current);

    return () => {
      socketIO.current.disconnect();
    }
  }, []);

  useEffect(() => {
    if (socket === undefined || quill === undefined) return;

    socket.once('load-document', document => {
      quill.setContents(document);
      quill.enable();
    });

    socket.emit('get-document', documentId);

  }, [socket, quill, documentId]);

  useEffect(() => {
    if (socket === undefined || quill === undefined) return;

    const interval = setInterval(() => {
      const inputStringLength = quill.getContents().ops[0].insert.length;

      if (inputStringLength > 1) {
        socket.emit('save-document', { quill: quill.getContents(), publisher: user.email });
      }
    }, 20000);

    return () => {
      clearInterval(interval);
    };
  }, [socket, quill]);

  useEffect(() => {
    if (socket === undefined || quill === undefined) return;

    const handler = (delta) => {
      quill.updateContents(delta);
    };

    socket.on('receive-changes', handler);

    return () => {
      quill.off('receive-changes', handler);
    }
  }, [socket, quill]);

  useEffect(() => {
    if (socket === undefined || quill === undefined) return;

    const handler = (delta, oldDelta, source) => {
      if (source !== 'user') return;

      socket.emit('send-changes', delta);
    };

    quill.on('text-change', handler);

    return () => {
      quill.off('text-change', handler);
    };
  }, [socket, quill]);

  useEffect(() => {
    if (socket === undefined || quill === undefined) return;

    const handler = (range, oldRange, source) => {
      if (source !== 'user') return;

      socket.emit('send-range-changes', range);
    };

    quill.on('selection-change', handler);

    return () => {
      quill.off('selection-change', handler);
    };
  }, [socket, quill]);

  useEffect(() => {
    if (socket === undefined || quill === undefined || cursor === undefined) return;

    const updateCursor = (range) => {
      cursor.moveCursor('cursor', range);
    };

    socket.on('receive-range-changes', updateCursor);

    return () => {
      quill.off('receive-range-changes', updateCursor);
    };
  });

  const wrapperRef = useCallback((wrapper) => {
    if (wrapper === null) return;

    const toolbarOptions = null;

    Quill.register('modules/cursors', QuillCursors);
    const container = document.createElement('div');
    wrapper.innerHTML = '';
    wrapper.append(container);
    const quillTemplate = new Quill(container, {
      modules: {
        toolbar: toolbarOptions,
        cursors: {
          transformOnTextChange: true,
        },
      },
      placeholder: 'write something...',
      theme: 'snow',
    });

    const quillCursor = quillTemplate.getModule('cursors');
    quillCursor.createCursor('cursor', `${user.displayName}`, 'red');
    setCursor(quillCursor);
    quillTemplate.on('selection-change', selectionChangeHandler(quillCursor));
    setQuill(quillTemplate);
  }, []);

  const handleSubmit = () => {
    socket.emit('save-document', {
      quill: quill.getContents(),
      publisher: user.email,
    });
    navigate('/');
  };

  function selectionChangeHandler(cursors) {
    const debouncedUpdate = debounce(updateCursor, 500);

    return function (range, oldRange, source) {

      if (source === 'user') {
        updateCursor(range);
      } else {
        debouncedUpdate(range);
      }
    };

    function updateCursor(range) {
      setTimeout(() => cursors.moveCursor('cursor', range), 50);
    }
  }

  function debounce(func, wait) {
    let timeout;

    return function (...args) {
      const context = this;
      const later = function () {
        timeout = null;
        func.apply(context, args);
      };
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
    };
  }

  const handleLogout = () => {
    auth.signOut();
    navigate('/');
  };

  const handleMoveMainPage = () => {
    navigate('/');
  };

  const handleDelete = async () => {
    try {
      await axios.delete('/page', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
        data: {
          documentId,
        },
      });

      navigate('/');
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <div>
      <h1>Document Page</h1>
      <Button margin={'5px'} width={'23.5%'} onClick={handleSubmit}>
        Save document
      </Button>
      <Button margin={'5px'} width={'23.5%'} onClick={handleDelete}>
        Delete document
      </Button>
      <Button margin={'5px'} width={'23.5%'} onClick={handleMoveMainPage}>
        To main page
      </Button>
      <Button margin={'5px'} width={'23.5%'} onClick={handleLogout}>
        👋 Sign out
      </Button>
      <QuillContainer>
        <div id='quill-input' ref={wrapperRef}></div>
      </QuillContainer>
    </div>
  );
}

const QuillContainer = styled.div`
  width: 100%;
  height: 70vh;

  #quill-input {
    height: 100%;
    background: white;
  }
`;
