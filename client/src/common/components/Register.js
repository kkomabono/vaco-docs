import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { auth } from '../../service/firebase';
import 'firebase/compat/auth';
import { createUserWithEmailAndPassword, updateProfile } from 'firebase/auth';

import styled from 'styled-components';
import LoadingSpin from './LoadingSpin';

export default function Register() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [displayName, setDisplayName] = useState('');
  const [isCompleted, setIsCompleted] = useState(true);
  const navigate = useNavigate();

  const onChangeInput = (event) => {
    const { target: { name, value } } = event;

    if (name === 'email') {
      setEmail(value);
    } else if (name === 'password') {
      setPassword(value);
    } else if (name === 'displayName') {
      setDisplayName(value);
    }
  }

  const handleSubmit = async (event) => {
    setIsCompleted(false);
    event.preventDefault();
    const joinedData = await createUserWithEmailAndPassword(auth, email, password);
    await updateProfile(auth.currentUser, { displayName });
    setIsCompleted(true);

    if (joinedData !== null) {
      navigate('/');
    }
  }

  return (
    <>
      <RegisterContainer>
        <div className='register fancy-item'>
          <div className='register-form-wrapper'>
            <form onSubmit={handleSubmit}>
              <h3>
                <label>Email</label>
              </h3>
              <div className='input-wrapper'>
                <input
                  className='register-input'
                  name='email'
                  type='email'
                  placeholder='Email'
                  required
                  onChange={onChangeInput}
                />
              </div>
              <h3>
                <label>Password</label>
              </h3>
              <div className='input-wrapper'>
                <input
                  className='register-input'
                  name='password'
                  type='password'
                  placeholder='Password'
                  minLength='8'
                  required
                  onChange={onChangeInput}
                />
              </div>
              <h3>
                <label>Name</label>
              </h3>
              <div className='input-wrapper'>
                <input
                  className='register-input'
                  name='displayName'
                  type='string'
                  placeholder='Name'
                  required
                  onChange={onChangeInput}
                />
              </div>
              <button className='register-button' type='submit'>
                Join
              </button>
              {!isCompleted && <LoadingSpin />}
            </form>
          </div>
        </div>
      </RegisterContainer>
    </>
  );
}

const RegisterContainer = styled.div`
  height: 80vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .register {
    width: fit-content;
    padding: 40px 50px;
    border-radius: 10px;
    box-shadow:
      0px 5px 8px 3px rgb(0 0 0 / 30%),
      0px 2px 5px -2px rgba(0, 0, 0, 0.418),
      0px 2px 5px -7px rgb(0 0 0 / 20%);
  }

  .register-form-wrapper {
    width: 360px;
  }

  .input-wrapper {
    box-shadow:
      0px 5px 8px 3px rgb(0 0 0 / 30%),
      0px 2px 5px -2px rgba(0, 0, 0, 0.418),
      0px 2px 5px -7px rgb(0 0 0 / 20%);
  }

  .register-input {
    font-size: 15px;
    width: 100%;
    padding: 14px 17px 13px;
    box-sizing: border-box;
  }

  .register-button {
    cursor: pointer;
    font-size: 16px;
    padding: 10px;
    margin: 20px 0px;
    width: 100%;
    box-sizing: border-box;
    float: left;
    transition-duration: 0.4s;
    background: white;
    border-radius: 2pt;
    border: 0;
    outline: 0;
    box-shadow:
      0px 5px 8px 3px rgb(0 0 0 / 30%),
      0px 2px 5px -2px rgba(0, 0, 0, 0.418),
      0px 2px 5px -7px rgb(0 0 0 / 20%);
  }

  .register-button:hover {
    background-color: rgba(159, 159, 159, 0.255);
    color: white;
  }

  .fancy-item {
    animation: animate 500ms 100ms cubic-bezier(0.33, 0.72, 0.51, 0.71) forwards;
    opacity: 0;
    transform: translateY(30px);
  }

  @keyframes animate {
    100% {
      opacity: 1;
      transform: none;
    }
  }
`;

