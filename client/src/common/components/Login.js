import React, { useState } from 'react';
import { signInWithGoogle } from '../../service/firebase';
import { useNavigate } from 'react-router-dom';
import styled from 'styled-components';
import './styles.css';

import { auth } from '../../service/firebase';
import { signInWithEmailAndPassword } from 'firebase/auth';
import 'firebase/compat/auth';

import Input from './shared/Input';
import LoadingSpin from './LoadingSpin';
import googleButton from '../../assests/googleButton.png';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isCompleted, setIsCompleted] = useState(true);
  const navigate = useNavigate();

  const onChangeInput = (event) => {
    const {
      target: { name, value },
    } = event;

    if (name === 'email') {
      setEmail(value);
    } else if (name === 'password') {
      setPassword(value);
    }
  }

  const handleSubmit = async (event) => {
    setIsCompleted(false);
    event.preventDefault();
    const successData = await signInWithEmailAndPassword(auth, email, password);
    setIsCompleted(true);

    if (successData !== null) {
      navigate('/');
    }
  };

  const handleMoveRegisterPage = () => {
    navigate('/register');
  };

  return (
    <>
      {!isCompleted && <LoadingSpin />}
      <LoginContainer>
        <LoginWrapper className='fancy-item'>
          <LoginFormWrapper>
            <form onSubmit={handleSubmit}>
              <InputWrapper>
                <Input
                  name='email'
                  type='email'
                  placeholder='Email'
                  required
                  onChange={onChangeInput}
                />
                <Input
                  name='password'
                  type='password'
                  placeholder='Password'
                  minLength='8'
                  required
                  onChange={onChangeInput}
                />
                <Button type='submit'>Login</Button>
              </InputWrapper>
            </form>
          </LoginFormWrapper>
          <div>
            <Button onClick={handleMoveRegisterPage} width={'360px'}>
              Join us
            </Button>
          </div>
          <MidWord>Or</MidWord>
          <div>
            <button className='google-button' onClick={signInWithGoogle}>
              <img
                className='google-image'
                src={googleButton}
                alt='googleButton'
              ></img>
            </button>
          </div>
        </LoginWrapper>
      </LoginContainer>
    </>
  );
}

const LoginContainer = styled.div`
  height: 80vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .fancy-item {
    animation: animate 500ms 100ms cubic-bezier(0.33, 0.72, 0.51, 0.71) forwards;
    opacity: 0;
    transform: translateY(30px);
  }

  @keyframes animate {
    100% {
      opacity: 1;
      transform: none;
    }
  }
`;

const LoginWrapper = styled.div`
  width: fit-content;
  padding: 50px 50px;
  border-radius: 10px;
  box-shadow:
    0px 5px 8px 3px rgb(0 0 0 / 30%),
    0px 2px 5px -2px rgba(0, 0, 0, 0.418),
    0px 2px 5px -7px rgb(0 0 0 / 20%);
`;

const LoginFormWrapper = styled.div`
  width: 360px;
`;

const InputWrapper = styled.div`
  box-shadow:
    0px 5px 8px 3px rgb(0 0 0 / 30%),
    0px 2px 5px -2px rgba(0, 0, 0, 0.418),
    0px 2px 5px -7px rgb(0 0 0 / 20%);
`;

const Button = styled.button`
  cursor: pointer;
  font-size: 16px;
  padding: 10px;
  margin: 20px 0px;
  width: 100%;
  width: ${(props) => props.width};
  box-sizing: border-box;
  float: left;
  transition-duration: 0.4s;
  background: white;
  border-radius: 3pt;
  border-color: white;
  box-shadow:
    0px 5px 8px 3px rgb(0 0 0 / 30%),
    0px 2px 5px -2px rgba(0, 0, 0, 0.418),
    0px 2px 5px -7px rgb(0 0 0 / 20%);

  &:hover {
    background-color: rgba(159, 159, 159, 0.255);
    color: white;
  }
`;

const MidWord = styled.p`
  text-align: center;
  font-size: 16px;
`;
