import React from 'react';
import { auth } from '../../service/firebase';
import { useNavigate } from 'react-router-dom';

import Button from './shared/Button';

export default function Error() {
  const search = window.location.search;
  const params = new URLSearchParams(search);
  const codeFromUrl = params.get('code');
  const navigate = useNavigate();

  const handleLogout = () => {
    auth.signOut();
    navigate('/');
  };

  const handleMoveToMainPage = () => {
    navigate('/');
  };

  return (
    <>
      <h1>에러 발생</h1>
      <div>{codeFromUrl}</div>
      <Button onClick={handleMoveToMainPage}>To main page</Button>
      <Button onClick={handleLogout}>Sign out</Button>
    </>
  );
}
