import React from 'react';
import styled from 'styled-components';
import banner from '../../assests/banner.png';

const Header = styled.header`
  position: relative;
  background-color: #ffffff;
  margin-top: 15px;
  margin-bottom: 25px;
  width: 100%;
  background: #f5f6f7;

  img {
    width: 350px;
  }
`;

export default function AppHeader() {
  return (
    <Header>
      <img src={banner} alt='banner' />
    </Header>
  );
}
