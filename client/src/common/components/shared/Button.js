import styled from 'styled-components'

export default styled.button`
  cursor: pointer;
  font-size: 16px;
  padding: 10px;
  margin: 20px 0px;
  margin: 20px ${(props) => props.margin};
  width: 35%;
  width: ${(props) => props.width};
  box-sizing: border-box;
  transition-duration: 0.4s;
  background: white;
  border-radius: 3pt;
  border: 0;
  outline: 0;
  box-shadow:
    0px 5px 8px 3px rgb(0 0 0 / 5%),
    0px 2px 5px -2px rgba(0, 0, 0, 0.118),
    0px 2px 5px -7px rgb(0 0 0 / 5%);

  &:hover {
    background-color: #eee;
    border-radius: 30px;
  }
`;
