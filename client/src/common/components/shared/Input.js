import styled from 'styled-components';

export default styled.input`
  font-size: 15px;
  width: 100%;
  padding: 14px 17px 13px;
  box-sizing: border-box;
`;
